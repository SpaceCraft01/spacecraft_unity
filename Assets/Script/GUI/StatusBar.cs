﻿using UnityEngine;
using System.Collections;

public class StatusBar : MonoBehaviour {

	public float barDisplay; //current progress
	public Vector2 pos = new Vector2(20,40);
	public Vector2 size = new Vector2(60,20);
	public Texture2D emptyTex;
	public Texture2D fullTex;


	private GUIStyle enerygyBar;

	void OnGUI() {
		enerygyBar = GUIStyle.none;
		//draw the background:
		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex,enerygyBar);
			//draw the filled-in part:
			GUI.BeginGroup(new Rect(0,0, size.x * barDisplay, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), fullTex,enerygyBar);
			GUI.EndGroup();
		GUI.EndGroup();
	}

	void Update() {

		GameObject player = GameObject.FindGameObjectWithTag("Player");
		if (player != null) {
			PlayerShip ship = player.GetComponent<PlayerShip>();
			barDisplay = ship.CurrentHealth / ship.MaxHealth;
		}
		else{
			barDisplay = 0f;
		}
	}
}
