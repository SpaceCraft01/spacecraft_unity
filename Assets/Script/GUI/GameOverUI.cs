﻿using UnityEngine;
using System.Collections;

public class GameOverUI : MonoBehaviour {


	public GUIText gameoverText;
	public GUIText scoreText;

	private bool _isGameOver;
	public bool IsGameOver{get{return _isGameOver;}}
	void Start()
	{
		_isGameOver = false;
	}
	
	// Update is called once per frame
	void Update () {

	}


	void OnGUI() {

		if(!_isGameOver)
			return;

		if (GUI.Button(new Rect( Screen.width/2 - 100, Screen.height * 0.65f - 15 ,200,30), "Play Again"))
		{
			Application.LoadLevel (Application.loadedLevelName);
			ScoreCounter.StartCount();
		}


		
	}

	public void setGameOver()
	{
		gameoverText.enabled = true;
		scoreText.enabled = true;
		scoreText.text = ScoreCounter.score.ToString ();

		_isGameOver = true;
	}


}
