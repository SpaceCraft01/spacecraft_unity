﻿using UnityEngine;
using System.Collections;

public class SkillButtons : MonoBehaviour {
	
	public float buttonWidth = 60;
	public float buttonHeight = 60;
	float horizontalMargin = 10;
	float bottomMargin = 5;

	public SkillBase [] skillArray;
	public Texture[] buttonTextures;

	private GameOverUI gameover;
	void Start()
	{
		gameover = GameObject.FindObjectOfType<GameOverUI> ();
	}
	void OnGUI() {

		if(gameover.IsGameOver){
			return;
		}

		if (buttonTextures.Length <= 0) {
			Debug.LogError("Please assign a texture on the inspector");
			return;
		}

		int numberOfButton = buttonTextures.Length;
		float xSkill = Screen.width / 2 - (numberOfButton * buttonWidth/ 2) - ((numberOfButton - 1) * horizontalMargin / 2);

		for (int i=0; i<numberOfButton; i++) {
			Rect btnRect = new Rect( xSkill + (buttonWidth+horizontalMargin) * i ,Screen.height - (buttonHeight + bottomMargin), buttonWidth,buttonHeight);

			var e = Event.current;
			if ((e.type == EventType.MouseDown) && btnRect.Contains (Event.current.mousePosition))
			{
				Debug.Log("Button ["+ i +"]" + " Mouse Down");
			}
			if (GUI.Button( btnRect, buttonTextures[i]))
			{
				Debug.Log("Button ["+ i +"]" + " Mouse Up");
				skillArray[i].Use();
			}
		}

	}

}
