﻿using UnityEngine;
using System.Collections;

public class SteeringManager {

	public Vector3 steering;
	public IBoid host;

//	private float MAX_FORCE;
	
	// The constructor
	public SteeringManager(IBoid host) {
		this.host = host;
		this.steering = new Vector3(0,0,0);

//		MAX_FORCE = this.host.MaxVelocity;
	}
	
	// The public API (one method for each behavior)
	public void Seek (Vector3 target)
	{
		steering += DoSeek (target);
	}

	public void Arrival(Vector3 target, float slowingRadius)
	{
		steering += DoSeek (target, slowingRadius);
	}

	public void Flee(Vector3 target)
	{
		steering += DoFlee (target);
	}
//	public void Wander();
//	public void Evade(IBoid target);
	public void Pursuit(IBoid target)
	{
		steering += DoPursuit (target);
	}
//	
	// The update method. 
	// Should be called after all behaviors have been invoked
	public void Update()
	{
		Vector3 velocity = host.Velocity;

//		#warning should know what is MAX_FORCE
//		steering = Truncate(steering, MAX_FORCE);
		steering *= 1f / host.Mass;
		velocity = steering;

//		#warning should know what is MaxVelocity use for
		velocity = Truncate(velocity, host.MaxVelocity);

		Rigidbody2D hostRigidbody2D = host.getRigidbody2D ();
		hostRigidbody2D.velocity = velocity;

		float newAngle = Mathf.Atan2(velocity.y,velocity.x) * Mathf.Rad2Deg - 90;
		Quaternion moveRotation = Quaternion.AngleAxis (newAngle, Vector3.forward);
		Transform hostTransform = host.getTransform ();
		hostTransform.rotation = Quaternion.RotateTowards (hostTransform.rotation, moveRotation, host.TurnSpeed * Time.deltaTime);
	}
//	
//	// Reset the internal steering force.
//	public void Reset();
//
//	// The internal API
	private Vector3 DoSeek (Vector3 target, float slowingRadius=0)
	{	
		Vector3 desired = target - host.Position;

		float distance = desired.magnitude;
		desired = desired.normalized;
		
		if (distance <= slowingRadius) {
			desired *= host.MaxVelocity * distance/slowingRadius;
		} else {
			desired *= host.MaxVelocity;
		}
		Vector3 force = desired - host.Velocity;
//		Debug.Log (force);
		return force;
	}
	private Vector3 DoFlee(Vector3 target)
	{
		return DoSeek(target) * -1;
	}
//	private void DoWander();
//	private void DoEvade(IBoid target);
	private Vector3 DoPursuit(IBoid target)
	{
		Vector3 distance = target.Position - host.Position;
		
		float updatesNeeded = distance.magnitude / host.MaxVelocity;
		Vector3 tv = target.Velocity;
		tv = tv * updatesNeeded;

		Vector3 targetFuturePosition = target.Position + tv;
		
		return DoSeek(targetFuturePosition);
	}


	Vector3 Truncate(Vector3 original,float max)
	{
		Vector3 truncatedVector = original;
		if (original.magnitude > max) {
			truncatedVector = original.normalized * max;
		}
		return truncatedVector;
	}



}
