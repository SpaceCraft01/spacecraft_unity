﻿using UnityEngine;
using System.Collections;

public class ShipMover : MonoBehaviour,IBoid {

	public float moveSpeed = 1f;
	public float turnSpeed = 65f;

	#region IBoid implements
	//IBoid Implements
	public Vector3 Velocity
	{
		get{ return rigidbody2D.velocity; }
	}
	public float MaxVelocity
	{
		get { return moveSpeed; }
	}
	public float TurnSpeed
	{
		get { return turnSpeed; }
	}
	public Vector3 Position
	{
		get{ return transform.position; }
	}
	public float Mass
	{
		get{ return rigidbody2D.mass; }
	}
	public Rigidbody2D getRigidbody2D()
	{
		return rigidbody2D;
	}
	public Transform getTransform ()
	{
		return transform;
	}
	#endregion

}
