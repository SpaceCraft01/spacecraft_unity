﻿using UnityEngine;
using System.Collections;

public interface IBoid
{

	Rigidbody2D getRigidbody2D();
	Transform getTransform();
	
	Vector3 Velocity{ get; }
	float TurnSpeed{get;}
	float MaxVelocity{ get; }
	Vector3 Position{ get; }
	float Mass{ get; }
}
