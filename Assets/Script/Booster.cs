﻿using UnityEngine;
using System.Collections;

public class Booster : MonoBehaviour {
	
	public float speedIncrease;
	public float turnRateIncrease;
	public float duration;

	public GameObject[] boosters;


	public AudioClip boosterSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	private ShipMover playerMover;
	private float startTime;
	private bool _isBoosting;

	// Use this for initialization
	void Start () {
		GameObject player = GameObject.FindWithTag("Player");
		playerMover = player.GetComponent<ShipMover> ();

		_isBoosting = false;

		for (int i = 0; i < boosters.Length; i++) {
			boosters[i].renderer.enabled = false;;
		}

	}
	
	// Update is called once per frame
	void Update () {

		if(!_isBoosting)
			return;

		if (Time.time - startTime >= duration) {
			playerMover.moveSpeed = playerMover.moveSpeed - speedIncrease;
			playerMover.turnSpeed = playerMover.turnSpeed - turnRateIncrease;
			_isBoosting = false;
			for (int i = 0; i < boosters.Length; i++) {
				boosters[i].renderer.enabled = false;	
			}
		}
	}

	public void Boost()
	{
		playerMover.moveSpeed = playerMover.moveSpeed + speedIncrease;
		playerMover.turnSpeed = playerMover.turnSpeed + turnRateIncrease;
		startTime = Time.time;
		_isBoosting = true;
		for (int i = 0; i < boosters.Length; i++) {
			boosters[i].renderer.enabled = true;	
		}

		AudioManager.Instance.audioSource.PlayOneShot( boosterSound,soundImpact);

	}
}
