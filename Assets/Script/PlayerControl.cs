﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {


	private ShipMover _shipmover;

	private Vector3 moveTarget;
	private Quaternion moveRotation;

	private Transform _transform;
	
	void Start () {
		_transform = this.transform;
		_shipmover = gameObject.GetComponent<ShipMover> ();
	}
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0) && GUIUtility.hotControl == 0) 
		{
			moveTarget = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			moveTarget.z = _transform.position.z;

			Vector3 lookPos = moveTarget - _transform.position;
			float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg - 90.0f;
			moveRotation = Quaternion.AngleAxis (angle, Vector3.forward);
		}

		Vector3 velocity = (moveTarget - _transform.position);
		if (velocity.magnitude > 0.1) {
			velocity = velocity.normalized * _shipmover.moveSpeed;
			velocity *= 1f / rigidbody2D.mass;
			rigidbody2D.velocity = velocity;
		}
		else{
			rigidbody2D.velocity = Vector3.zero;
		}

		_transform.rotation = Quaternion.RotateTowards (_transform.rotation, moveRotation, _shipmover.turnSpeed * Time.deltaTime);

	}


}
