﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject distantObject;
	public GameObject shipToSpawn;
	public float durationToSpawn;

	public float durationToIncreaseSpawnRate;
	public float percentToIncreaseSpawnRate;

	private float radius;
	private float startTime;

	private float increaseDurationStartTime;
	// Use this for initialization
	void Start () {
		radius = Vector3.Distance (distantObject.transform.position, gameObject.transform.position);
		startTime = Time.time;
		increaseDurationStartTime = Time.time;
	}

	// Update is called once per frame
	void Update () {
	
		if (Time.time - startTime > durationToSpawn) {
			//spawn

			Vector3 randomPoint = Random.insideUnitCircle.normalized * radius;
			randomPoint += gameObject.transform.position;

			Quaternion randRoration = Random.rotation;
			randRoration.x = 0;
			randRoration.y = 0;


			Instantiate( shipToSpawn , randomPoint , randRoration);
	
			startTime = Time.time;
		}

		if (Time.time - increaseDurationStartTime > durationToIncreaseSpawnRate) {
			durationToSpawn = durationToSpawn - durationToSpawn*percentToIncreaseSpawnRate/100f;
			increaseDurationStartTime = Time.time;
		}



	}
}
