﻿using UnityEngine;
using System.Collections;

public class EnemyProjectile : Projectile {

	public AudioClip shotSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	public AudioClip reflectSound;
	[Range(0,1)]
	public float reflectSoundImPact = 1f;

	void OnTriggerEnter2D(Collider2D collider){

		if (collider.tag == "Player") {

			PlayerShip player = collider.gameObject.GetComponent<PlayerShip>();
			player.Hurt( attack );

			GameObject explosion = Resources.Load("projectileExplosion") as GameObject;
			Instantiate( explosion , transform.position , transform.rotation);

			AudioManager.Instance.audioSource.PlayOneShot( shotSound,soundImpact);


			Destroy(gameObject);
		}

		if (collider.tag == "Shield") {
		
			Shield shield = collider.gameObject.GetComponent<Shield>();
			shield.Hurt(attack);

			GameObject explosion = Resources.Load("projectileExplosion") as GameObject;
			Instantiate( explosion , transform.position , transform.rotation);

			AudioManager.Instance.audioSource.PlayOneShot( reflectSound,reflectSoundImPact);


			Destroy(gameObject);
		}

	}

}
