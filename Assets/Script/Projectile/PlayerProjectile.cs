﻿using UnityEngine;
using System.Collections;

public class PlayerProjectile : Projectile {

	public AudioClip shotSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	void OnTriggerEnter2D(Collider2D collider){

		if (collider.tag == "Enemy") {
			Ship enemyShip = collider.gameObject.GetComponent<Ship>();
			enemyShip.Hurt( attack );

			GameObject explosion = Resources.Load("projectileExplosion") as GameObject;
			Instantiate( explosion , transform.position , transform.rotation);

			AudioManager.Instance.audioSource.PlayOneShot( shotSound,soundImpact);

			Destroy(gameObject);
		}

	}

}
