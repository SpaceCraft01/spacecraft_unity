﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {


	public AudioSource audioSource;
	//Here is a private reference only this class can access
	private static AudioManager _instance;
	
	//This is the public reference that other classes will use
	public static AudioManager Instance
	{
		get
		{
			//If _instance hasn't been set yet, we grab it from the scene!
			//This will only happen the first time this reference is used.
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<AudioManager>();
			return _instance;
		}
	}
}
