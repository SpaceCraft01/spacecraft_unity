﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour {

	public GameObject projectile;
	public float delay = 0.3f;

	public AudioClip shootSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	private bool canShoot;
	private float nextShootTime;
	// Use this for initialization
	void Start ()
	{
		canShoot = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Time.time > nextShootTime) {
			canShoot = true;		
		}
	}

	public void Shoot()
	{
		if(canShoot){
			Instantiate (projectile, transform.position, transform.rotation);

			AudioManager.Instance.audioSource.PlayOneShot( shootSound,soundImpact);

			canShoot = false;
			nextShootTime = Time.time + delay;

		}
	}
}
