﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {


	public float HP = 100;
	public AudioClip shieldActivatedSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	private float _hp;
	// Use this for initialization
	void Start () {
		renderer.enabled = false;
		collider2D.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void activateShield()
	{
		_hp = HP;
		renderer.enabled = true;
		collider2D.enabled = true;

		AudioManager.Instance.audioSource.PlayOneShot (shieldActivatedSound , soundImpact);
	}

	public void Hurt(float damage)
	{
		_hp -= damage;
		if (_hp <= 0) {
			renderer.enabled = false;
			collider2D.enabled = false;
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Enemy") {
			Ship enemyShip = collider.GetComponent<Ship>();
			this.Hurt(enemyShip.StrikeDamage);

			enemyShip.Hurt( 100000f );
		}
	}
}
