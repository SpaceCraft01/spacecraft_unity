﻿using UnityEngine;
using System.Collections;

public class ScoreCounter : MonoBehaviour {

	public GUIText scoreText;
	public static int score = 0;


	private static bool isCounting;
	private GameOverUI gameover;
	// Use this for initialization
	void Start () {
		isCounting = true;
		scoreText.text = 0 + "";

		gameover = GameObject.FindObjectOfType<GameOverUI> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(gameover.IsGameOver){
			scoreText.enabled = false;
		}
		if (isCounting) {
			score += (int)(Time.deltaTime * 100);
			scoreText.text = score.ToString ();
		}
	}
	
	public static void addScore(int _score)
	{
		if(!isCounting)
			return;
		score += _score;
	}

	public static void StartCount()
	{
		score = 0;
		isCounting = true;
	}
	public static void StopCount()
	{
		isCounting = false;
	}
}
