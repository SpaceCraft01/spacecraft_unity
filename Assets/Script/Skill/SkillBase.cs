﻿using UnityEngine;
using System.Collections;

public abstract class SkillBase : MonoBehaviour {

	public float CoolDown;

	public abstract bool CanUse{ get;}
	public abstract bool IsUsing{ get;}
	public abstract float CurrentProgress{ get;}
	public abstract void Use ();
}
