﻿using UnityEngine;
using System.Collections;

public class ShieldSkill : SkillBase {

	public Shield shield;

	private bool _canUse;
	private float _startTime;

	// Use this for initialization
	void Awake()
	{
		_canUse = true;
	}
	void Update()
	{
		if (Time.time - _startTime > base.CoolDown) {
			_canUse = true;		
		}
	}

	#region implemented abstract members of SkillBase
	public override void Use ()
	{
		if(_canUse){

			shield.activateShield();

			_canUse = false;
			_startTime = Time.time;
		}
	}
	public override bool CanUse {
		get {
			return _canUse;
		}
	}
	public override bool IsUsing {
		get {
			return !_canUse;
		}
	}
	public override float CurrentProgress {
		get {
			return (Time.time - _startTime) / base.CoolDown;
		}
	}
	#endregion
}
