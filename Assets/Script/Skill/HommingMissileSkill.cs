﻿using UnityEngine;
using System.Collections;

public class HommingMissileSkill : SkillBase {

	public HomingMissile Missile;
	public DetectAndSetTarget shootBox;

	private bool _canUse;
	private float _startTime;


	//sound
	public AudioClip shootSound;
	[Range(0,1)]
	public float soundImpact = 1f;


	void Awake()
	{
		_canUse = true;
	}
	void Update()
	{
		if (Time.time - _startTime > base.CoolDown) {
			_canUse = true;		
		}
	}
	#region implemented abstract members of SkillBase
	public override void Use ()
	{
		if(shootBox.Target != null && _canUse){
			HomingMissile newMissile = (HomingMissile)Instantiate( Missile , transform.position , transform.rotation);
			newMissile.setTarget(shootBox.Target);

			AudioManager.Instance.audioSource.PlayOneShot( shootSound,soundImpact);

			_canUse = false;
			_startTime = Time.time;
		}
	}
	public override bool CanUse {
		get {
			return _canUse;
		}
	}
	public override bool IsUsing {
		get {
			return !_canUse;
		}
	}
	public override float CurrentProgress {
		get {
			return (Time.time - _startTime) / base.CoolDown;
		}
	}
	#endregion

}
