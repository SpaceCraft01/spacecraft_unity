﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	public int score = 1000;
	public float Health = 50;
	public float StrikeDamage = 40;

	public AudioClip explosionSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	public void Update()
	{
		if (Health <= 0) {
			//explosion
			GameObject explosion = Resources.Load("shipExplosion") as GameObject;
			Instantiate(explosion,transform.position,transform.rotation);
			ScoreCounter.addScore(score);
			Destroy (gameObject);		

			AudioManager.Instance.audioSource.PlayOneShot( explosionSound,soundImpact);
		}
	}
	public void Hurt(float damage)
	{
		Health -= damage;
	}
}
