﻿using UnityEngine;
using System.Collections;

public class HomingMissile : MonoBehaviour {

	public float attack = 50f;

	private ShipMover _shipMover;
	private SteeringManager _steering;

	private Transform _targetTransform;

	// Use this for initialization
	void Start () {
		_shipMover = gameObject.GetComponent<ShipMover> ();
		_steering = new SteeringManager(_shipMover);
	}


	public void setTarget(GameObject target)
	{
		_targetTransform = target.transform;
	}


	// Update is called once per frame
	void Update ()
	{
		if (_targetTransform == null) {
			//no target		
		}
		else {
			_steering.Seek(_targetTransform.position);
			_steering.Update();
		}
	}

	void OnTriggerEnter2D(Collider2D collider){
		
		if (collider.tag == "Enemy") {
			Ship enemyShip = collider.gameObject.GetComponent<Ship>();
			enemyShip.Hurt( attack );
			
			GameObject explosion = Resources.Load("projectileExplosion") as GameObject;
			Instantiate( explosion , transform.position , transform.rotation);
			
			Destroy(gameObject);
		}
		
	}


}
