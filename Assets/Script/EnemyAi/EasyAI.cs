﻿using UnityEngine;
using System.Collections;

public class EasyAI : MonoBehaviour {

	private SteeringManager steering;
	private Transform _playerTransform;
	private ShipMover _shipMover;

	// Use this for initialization
	void Start () {
		//init steering
		GameObject player = GameObject.FindGameObjectWithTag ("Player");
		_playerTransform = player.transform;
		_shipMover = player.GetComponent<ShipMover> ();

		ShipMover shipMoverComponent = gameObject.GetComponent<ShipMover> ();
		steering = new SteeringManager(shipMoverComponent);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (_shipMover != null) {

//			steering.Flee (_playerTransform.position);
			steering.Pursuit (_shipMover);
//			steering.Seek (_playerTransform.position);
//			steering.Arrival(_playerTransform.position , 1);
			steering.Update ();

		}
	}
}
