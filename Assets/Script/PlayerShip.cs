﻿using UnityEngine;
using System.Collections;

public class PlayerShip : MonoBehaviour {
	
	public float MaxHealth;

	public float regenPercentPerSecond = 1;

	private float _currentHealth;
	public float CurrentHealth {get{return _currentHealth;}}

	public AudioClip explosionSound;
	[Range(0,1)]
	public float soundImpact = 1f;

	// Use this for initialization
	void Start () {
		_currentHealth = MaxHealth;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (_currentHealth <= 0) {
			GameObject explosion = Resources.Load("shipExplosion") as GameObject;
			Instantiate(explosion,transform.position,transform.rotation);

			ScoreCounter.StopCount();

			Destroy (gameObject);	

			AudioManager.Instance.audioSource.PlayOneShot( explosionSound,soundImpact);

			GameOverUI gameover = GameObject.FindObjectOfType<GameOverUI>();
			gameover.setGameOver();
		}

		float newHp = _currentHealth + regenPercentPerSecond * Time.deltaTime;
		if (newHp <= MaxHealth){
			_currentHealth = newHp;
		}
	}

	public void Hurt(float damage)
	{
		_currentHealth -= damage;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Enemy") {
			Ship enemyShip = collider.GetComponent<Ship>();
			this.Hurt(enemyShip.StrikeDamage);

			enemyShip.Hurt( 100000f );
		}
	}
}
