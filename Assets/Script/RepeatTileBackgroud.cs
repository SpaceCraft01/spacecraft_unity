﻿using UnityEngine;
using System.Collections;

public class RepeatTileBackgroud : MonoBehaviour {

	public float widthScale;
	public float heightScale;
	public GameObject tileBackground;
	public GameObject backGroundParent;

	private Transform _cameraTransform;
	private int COLS;
	private int ROWS;
	private int currentColumn;
	private int currentRow;
	private GameObject [,] tiles;

	// Use this for initialization
	void Start () {
		_cameraTransform = camera.transform;

		var orthographicSizeHeight = camera.orthographicSize;
		var orthographicSizeWidth = orthographicSizeHeight * camera.aspect;

		COLS = Mathf.CeilToInt(orthographicSizeWidth/widthScale) * 2 + 1;
		ROWS = Mathf.CeilToInt(orthographicSizeHeight/heightScale) * 2 + 1;

		tiles = new GameObject[ROWS,COLS];

		initTiles ();
	}

	// Update is called once per frame
	void Update () {
		ArrangeHorizontalTile ();
		ArrangeVerticalTile ();
	}

	#region Arrange Tile
	void ArrangeHorizontalTile ()
	{
		int tilePos_X = getTilePositionX ();
		if (currentColumn != tilePos_X) {
			int diff = tilePos_X - currentColumn;
			if (diff > 0)
				PutLeftTileToRightSide (tilePos_X);
			else
				PutRightTileToLeftSide (tilePos_X);
			currentColumn = tilePos_X;
		}
	}

	void ArrangeVerticalTile ()
	{
		int tilePos_Y = getTilePositionY ();
		if (currentRow != tilePos_Y) {

			int diff = tilePos_Y - currentRow;

			if (diff > 0)
				PutBottomTileToTop (tilePos_Y);
			else
				PutTopTileToBottom(tilePos_Y);

			currentRow = tilePos_Y;
		}
	}
	void PutTopTileToBottom(int tilePosY)
	{
		//  [ T1 , T2 , T3 ]  ->  [ A  , B  , C  ]
		//  [ A  , B  , C  ]  ->  [ D  , E  , F  ]
		//  [ D  , E  , F  ]  ->  [ T1 , T2 , T3 ] 
		for (int col = 0; col < COLS; col++) {
			
			GameObject topTile = tiles[0,col];
			
			for (int row = 0; row <= ROWS-2; row++) {
				tiles[row,col] = tiles[row+1,col];
			}
			tiles[ROWS-1,col] = topTile;
			
			float posY = (tilePosY - ROWS / 2) * heightScale;
			Vector3 newPos = new Vector3 ( topTile.transform.position.x, posY, topTile.transform.position.z);
			
			topTile.transform.position = newPos;
		}
	}

	void PutBottomTileToTop(int tilePosY)
	{
		//  [ A  , B  , C  ]  ->  [ U1 , U2 , U3 ]
		//  [ D  , E  , F  ]  ->  [ A  , B  , C  ]
		//  [ U1 , U2 , U3 ]  ->  [ D  , E  , F  ]
		for (int col = 0; col < COLS; col++) {

			GameObject bottomTile = tiles[ROWS-1,col];

			for (int row = ROWS-1; row >= 1; row--) {
				tiles[row,col] = tiles[row-1,col];
			}
			tiles[0,col] = bottomTile;

			float posY = (tilePosY + ROWS / 2) * heightScale;
			Vector3 newPos = new Vector3 ( bottomTile.transform.position.x, posY, bottomTile.transform.position.z);
			
			bottomTile.transform.position = newPos;
		}
	}
	void PutLeftTileToRightSide(int tilePosX)
	{
		//  [L1 , A , B , C ]  ->  [ A , B , C , L1]
		//  [L2 , D , E , F ]  ->  [ D , E , F , L2]
		//  [L3 , G , H , I ]  ->  [ G , H , I , L3]

		for (int row = 0; row < ROWS; row++) 
		{
			GameObject leftTile = tiles[row,0];
			for (int col = 0; col <= COLS-2; col++) {
				tiles[row,col] = tiles [row,col+1];
			}
			tiles [row,COLS - 1] = leftTile;

			float posX = (tilePosX + COLS / 2) * widthScale;
			Vector3 newPos = new Vector3 ( posX, leftTile.transform.position.y, leftTile.transform.position.z);

			leftTile.transform.position = newPos;
		}
	}
	void PutRightTileToLeftSide(int tilePosX)
	{
		//  [ A , B , C , r1 ]  ->  [ r1 , A , B , C ]
		//  [ D , E , F , r2 ]  ->  [ r2 , D , E , F ]
		//  [ G , H , I , r3 ]  ->  [ r3 , G , H , I ]

		for (int row = 0; row < ROWS; row++) 
		{
			GameObject rightTile = tiles [row,COLS-1];

			for (int col = COLS-1; col >= 1; col--) {
				tiles[row,col] = tiles [row,col-1];
			}
			tiles[row,0] = rightTile;

			float posX = (tilePosX - COLS/2) * widthScale;
			Vector3 newPos = new Vector3 ( posX , rightTile.transform.position.y , rightTile.transform.position.z);
			rightTile.transform.position = newPos;
		}
	}
	#endregion
	void initTiles()
	{
		for (int i = 0; i < ROWS; i++)
		{
			int vertical_Index = ROWS/2 - i;
			for (int j = 0; j < COLS; j++)
			{
				int horizontal_Index = j - COLS/2;

				float xPos = horizontal_Index * widthScale;
				float yPos = vertical_Index * heightScale;

				Vector3 pos = new Vector3( xPos , yPos , tileBackground.transform.position.z );
				GameObject newBackground = (GameObject)Instantiate( tileBackground , pos , tileBackground.transform.rotation);
				newBackground.transform.parent = backGroundParent.transform;

				tiles[i,j] = newBackground;
				tiles[i,j].name = "bg " + i + " " + j;
			}
		}
	}

	int getTilePositionX()
	{
		float cameraX = _cameraTransform.position.x;
		cameraX = cameraX < 0f ? cameraX - widthScale : cameraX;
		int currentPos_X = (int)(cameraX / widthScale);

		return currentPos_X;
	}
	int getTilePositionY()
	{
		float cameraY = _cameraTransform.position.y;
		cameraY = cameraY < 0f ? cameraY - heightScale : cameraY;
		int currentPos_Y = (int)(cameraY / heightScale);

		return currentPos_Y;
	}
}