﻿using UnityEngine;
using System.Collections;

public class DetectPlayerAndShoot : MonoBehaviour {

	public GameObject AutomaticGun;

	private AutomaticGun automaticGunComponent;

	// Use this for initialization
	void Start () {
		automaticGunComponent = AutomaticGun.GetComponent<AutomaticGun>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D(Collider2D collider)
	{	
		if (collider.tag == "Player"){
			automaticGunComponent.Shoot();
		}
	}
}
