﻿using UnityEngine;
using System.Collections;

public class AutomaticGun : MonoBehaviour
{

	public GameObject[] Guns;

	private Gun[] GunComponents;

	// Use this for initialization
	void Start ()
	{
		GunComponents = new Gun[Guns.Length];
		for (int i = 0; i < Guns.Length; i++) {
			GunComponents[i] = Guns[i].GetComponent<Gun>();	
		}
	}

	public void Shoot()
	{
		for (int i = 0; i < Guns.Length; i++) {
			GunComponents[i].Shoot();
		}
	}
}
