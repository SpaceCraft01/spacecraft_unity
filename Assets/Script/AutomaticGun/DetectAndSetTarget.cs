﻿using UnityEngine;
using System.Collections;

public class DetectAndSetTarget : MonoBehaviour {
	
	public GameObject Target{ get{return _target;} }
	private GameObject _target;
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Enemy")
		{
			if(_target == null)
				_target = collider.gameObject;
		}
	}
	void OnTriggerStay2D(Collider2D collider)
	{
		if (collider.tag == "Enemy")
		{
			if(_target == null)
				_target = collider.gameObject;
		}
	}
	void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.tag == "Enemy")
		{
			if(collider.gameObject == _target)
				_target = null;
		}
	}
}
