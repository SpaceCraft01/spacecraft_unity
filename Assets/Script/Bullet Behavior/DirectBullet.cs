﻿using UnityEngine;
using System.Collections;

public class DirectBullet : MonoBehaviour {

	public float speed = 1.0f;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate( Vector3.up * speed * Time.deltaTime);
	}

	void OnBecameInvisible() {
		Destroy(this.gameObject);
	}
}
