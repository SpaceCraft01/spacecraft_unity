﻿using UnityEngine;
using System.Collections;

public class SinWaveBullet : MonoBehaviour {

	public bool IsInvertDirection;
	public float speed = 4f;
	public float amplitude = 2f;
	public float omega = 10f;
	public float phase = Mathf.PI;

	private float _startTime;
	private float x;
	private float t;
	// Use this for initialization
	void Start () {
		_startTime = Time.time;
	}

	void FixedUpdate()
	{
		t = Time.time - _startTime;
		x = amplitude * (Mathf.Sin (omega * t + phase));
	}
	// Update is called once per frame
	void Update () {

		Vector3 translateVector = new Vector3( IsInvertDirection ? -x : x , 1 , transform.position.z );
		transform.Translate(translateVector * Time.deltaTime * speed);

	}
	
	void OnBecameInvisible() {
		Destroy(this.gameObject);
	}
}
